package cn.turing.firecontrol.device.mapper;

import cn.turing.firecontrol.common.data.Tenant;
import cn.turing.firecontrol.device.entity.DeviceDataDictionary;
import cn.turing.firecontrol.common.mapper.CommonMapper;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 
 * 
 * @author bjws
 * @email bjws@163.com
 * @version 2021/3/25 09:17:57
 */
@Tenant
@CacheNamespace(implementation=cn.turing.firecontrol.device.config.MybatisRedisCache.class,size = 1024)
public interface DeviceDataDictionaryMapper extends CommonMapper<DeviceDataDictionary> {
	
}
