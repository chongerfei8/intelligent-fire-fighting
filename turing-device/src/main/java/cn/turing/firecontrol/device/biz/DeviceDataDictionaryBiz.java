package cn.turing.firecontrol.device.biz;

import org.springframework.stereotype.Service;

import cn.turing.firecontrol.device.entity.DeviceDataDictionary;
import cn.turing.firecontrol.device.mapper.DeviceDataDictionaryMapper;
import cn.turing.firecontrol.common.biz.BusinessBiz;

/**
 * 
 *
 * @author bjws
 * @email bjws@163.com
 * @version 2021/3/25 09:17:57
 */
@Service
public class DeviceDataDictionaryBiz extends BusinessBiz<DeviceDataDictionaryMapper,DeviceDataDictionary> {
}