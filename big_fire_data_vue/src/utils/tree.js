/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

export function getAllChildrenIds(ids, data) {
  if (data.children !== undefined) {
    for (let i = 0; i < data.children.length; i++) {
      ids.push(data.children[i].id);
      const child = data.children[i]
      getAllChildrenIds(ids, child)
    }
  } else {
    return;
  }
}
