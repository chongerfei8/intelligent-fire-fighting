import request from '@/utils/request'
// 视频分析页面
export function getSumtimes() {
    return request({
      url: '/api/device/videoAnalysisSolution/solutionStatus',
      method: 'get'
    })
}
export function getAnaplan() {
    return request({
      url: '/api/device/videoAnalysisSolution/solutions',
      method: 'get'
    })
}
// 火焰识别 查看所有的设备
export function checkDev() {
    return request({
      url: '/api/device/deviceVideoGroup/all',
      method: 'get',
    })
}
// 查询设备列表
export function page(query) {
    return request({
      url: '/api/device/deviceVideoExt/queryAll',
      method: 'get',
      params: query
    })
  }
  // 配置按钮
export function configSolution(obj) {
    return request({
      url: '/api/device/deviceVideoExt/configSolution',
      method: 'post',
      data: obj
    })
}