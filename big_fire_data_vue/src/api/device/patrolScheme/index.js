/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

//巡检计划列表
export function page(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/pageList',
    method: 'get',
    params: query
  })
}

//巡检计划
export function addObj(obj) {
  return request({
    url: '/api/device/deviceInspectionScheme/add',
    method: 'post',
    data: obj
  })
}

//编辑添加巡检计划
export function addQuery(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/get',
    method: 'get',
    params: query
  })
}

//编辑巡检计划
export function updateObj(obj) {
  return request({
    url: '/api/device/deviceInspectionScheme/update',
    method: 'post',
    data: obj
  })
}

//删除巡检计划
export function deleteObj(query) {
  return request({
    url: '/api/device/deviceInspectionScheme/delete',
    method: 'get',
    params: query
  })
}
