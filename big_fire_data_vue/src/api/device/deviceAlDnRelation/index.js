/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
return request({
url: '/api/device/deviceAlDnRelation/pageList',
method: 'get',
params: query
})
}

export function addObj(obj) {
return request({
url: '/api/device/deviceAlDnRelation',
method: 'post',
data: obj
})
}

export function getObj(id) {
    return request({
    url: '/api/device/deviceAlDnRelation/get?id=' + id,
    method: 'get'
    })
}

export function delObj(id) {
return request({
url: '/api/device/deviceAlDnRelation/' + id,
method: 'delete'
})
}

export function putObj(obj) {
return request({
url: '/api/device/deviceAlDnRelation/updateOrAdd',
method: 'post',
data: obj
})
}
