/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function getRatioByDate(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getRatioByDate',
    method: 'get',
    params: query
  })
}
export function selectCountByType(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/selectCountByType',
    method: 'get',
    params: query
  })
}
export function selectFaultCountByMonth(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/selectFaultCountByMonth',
    method: 'get',
    params: query
  })
}
