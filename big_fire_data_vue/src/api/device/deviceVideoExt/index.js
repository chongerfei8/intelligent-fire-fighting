import request from '@/utils/request'
// 获取右侧设备组列表
export function getDeviceTree() {
    return request({
      url: '/api/device/deviceVideoGroup/all ',
      method: 'get'
    })
}
export function page(query) {
  return request({
    url: '/api/device/deviceVideoExt/queryList',
    method: 'get',
    params: query
  })
}
// 高级查询下拉框
export function advanceSelected(query) {
  return request({
    url: '/api/device/deviceSensor/getSelected',
    method: 'get',
    params: query
  })
}
// 添加传感器厂商的下拉框
export function addSelected(query) {
  return request({
    url: '/api/device/deviceSensorType/selectType',
    method: 'get',
    params: query
  })
}
// 添加传感器确定按钮
export function addSensor(obj) {
  return request({
    url: '/api/device/deviceVideoExt/save',
    method: 'post',
    data: obj
  })
}
// 删除设备
export function deleteDevices(query) {
  return request({
    url: '/api/device/deviceVideoExt/deleteDevices',
    method: 'get',
    params: query
  })
}
// 编辑设备
export function checkDevice(query) {
  return request({
    url: '/api/device/deviceVideoExt/queryById',
    method: 'get',
    params: query
  })
}
// 更新传感器
export function updateObj(obj) {
  return request({
    url: '/api/device/deviceVideoExt/update',
    method: 'post',
    data: obj
  })
}
// 添加设备组确定
export function addVideoGroupcon(obj) {
  return request({
    url: '/api/device/deviceVideoGroup',
    method: 'post',
    data: obj
  })
}
// 删除设备组确定
export function delVideoGroup(id) {
  return request({
  url: '/api/device/deviceVideoGroup/' + id,
  method: 'delete'
  })
}
// 编辑设备组
export function editVideoGroup(id) {
  return request({
  url: '/api/device/deviceVideoGroup/' + id,
  method: 'get'
  })
}
// 更新设备组
export function updateVideoGroup(id,obj) {
  return request({
  url: '/api/device/deviceVideoGroup/' + id,
  method: 'put',
  data: obj
  })
}