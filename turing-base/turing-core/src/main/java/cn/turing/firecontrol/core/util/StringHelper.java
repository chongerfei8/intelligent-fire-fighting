package cn.turing.firecontrol.core.util;

/**
 * Created by bjws on 2021/3/25.
 */
public class StringHelper {
    public static String getObjectValue(Object obj){
        return obj==null?"":obj.toString();
    }
}
