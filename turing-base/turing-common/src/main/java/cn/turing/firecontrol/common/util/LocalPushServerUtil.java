package cn.turing.firecontrol.common.util;

import cn.turing.firecontrol.common.constant.MessageConst;
import cn.turing.firecontrol.common.enums.EncodeEnum;
import cn.turing.firecontrol.common.enums.TypeEnum;
import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * @Description 本地推送服务
 * @Author gongxuan
 * @Date 2020/06/05 11:24
 * @Version V3.2
 */
@Slf4j
@Getter
@Setter
public class LocalPushServerUtil {

    private String url;

    public static void main(String[] args) {

        String[] phoneArr = {"13072763393"};

        LocalPushServerUtil localPushServerUtil = new LocalPushServerUtil();
        localPushServerUtil.setUrl(MessageConst.LOCAL_SERVER_URL);
        //localPushServerUtil.sendNotePost(phoneArr,TypeEnum.SMS,EncodeEnum.UTF8,"龚玄给你发的测试短信");

        localPushServerUtil.sendNotePost(phoneArr,TypeEnum.Broadcast,EncodeEnum.UTF8,"这是北京维数的一条测试语音");
    }

    /**
     * 发送警报短信或者警报电话
     * @param receiveArray  接收警报的手机号或者邮箱，当类型为sms或者call的时候填写手机号，当为email的时候填写邮箱
     * @param typeEnum      告警通知类型SMS-短信通知   Call-电话语音  All-电话语音和短信  Email-邮件通知发送
     * @param encodeEnum    短信的编码方式，默认为UTF-8
     * @param text          短信或者语音发送的内容
     * @return              返回发送的结果
     */
    public String sendNotePost(String[] receiveArray, TypeEnum typeEnum, EncodeEnum encodeEnum, String text) {

        url = MessageConst.LOCAL_SERVER_URL;

        JSONObject paramJson = new JSONObject();

        paramJson.put(MessageConst.LOCAL_SERVER_TO, receiveArray);
        paramJson.put(MessageConst.LOCAL_SERVER_TYPE, typeEnum.getWarnType());
        paramJson.put(MessageConst.LOCAL_SERVER_ENCODING, encodeEnum.getEncodeType());
        paramJson.put(MessageConst.LOCAL_SERVER_Text, text);

        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        StringBuilder result = new StringBuilder();
        try {

            URL realUrl = new URL(url);

            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();

            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);

            // 获取URLConnection对象对应的输出流
            printWriter = new PrintWriter(conn.getOutputStream());

            // 发送请求参数
            printWriter.print(paramJson.toString());

            // flush输出流的缓冲
            printWriter.flush();

            // 定义BufferedReader输入流来读取URL的响应
            bufferedReader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        log.info("接收到的返回值为:{}", result.toString());

        JSONObject json = JSONObject.parseObject(result.toString());
        return json.get("Reply").toString();
    }

    public LocalPushServerUtil() {
    }
}
