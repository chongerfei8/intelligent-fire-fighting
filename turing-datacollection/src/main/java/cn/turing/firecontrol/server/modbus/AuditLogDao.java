package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.AuditLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AuditLogDao {
    public static final String DELETE = "DELETE";
    public static final String ADD = "ADD";
    public static final String UPDATE = "UPDATE";
    int insert(AuditLog record);
    int insertSelective(AuditLog record);
    AuditLog selectByPrimaryKey(Integer logNum);
    int updateByPrimaryKeySelective(AuditLog record);
    int updateByPrimaryKey(AuditLog record);
    int deleteByPrimaryKey(Integer logNum);
}
