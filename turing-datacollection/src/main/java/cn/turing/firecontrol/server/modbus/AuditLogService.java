package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.AuditLog;

public interface AuditLogService {
    int deleteByPrimaryKey(Integer logNum);

    int insert(AuditLog record);

    int insertSelective(AuditLog record);

    AuditLog selectByPrimaryKey(Integer logNum);

    int updateByPrimaryKeySelective(AuditLog record);

    int updateByPrimaryKey(AuditLog record);
}
